import os

def input_dict():
    path = str(input('Введите путь к папке: '))
    return path

def create_dict(path, dict1 = {}):
    for i in os.listdir(path):
        try:
            if os.path.isdir(path+"/"+i):
                create_dict(path+"/"+i)
            else:
                dict1[path+"/"+ i]= os.stat(path+"/"+ i).st_size
        except PermissionError:
            pass
    num_files = len([f for f in os.listdir(path)
                if os.path.isfile(os.path.join(path, f))])
    return dict1


def analyze_dict(d):
    result = {}
    for path, size in d.items():
        name = os.path.basename(path)
        if (size, name) in result:
            result[(size, name)].append(path)
        else:
            result[(size, name)] = [path]
    return {k: v for k, v in result.items() if len(v) > 1}

def duplicate_files(duplicates):
    if len(duplicates)!=0:
        for filename in duplicates.keys():
            print(filename)
            for path in duplicates[filename]:
                print(path)
    else:
        print('Повторяющиеся файлы не найдены')

if __name__ == "__main__":
	duplicate_files(analyze_dict(create_dict(input_dict())))
